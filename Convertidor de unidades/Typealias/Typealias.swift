//
//  File.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 19/02/21.
//

import Foundation

typealias onError = (String) -> Void
typealias onSuccess =  (CoinsData) -> Void
