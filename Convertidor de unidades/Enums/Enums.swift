//
//  Enums.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 13/02/21.
//

import Foundation

enum UnitConversion {
    
    case distance, weight, badge, bet, time, temperature
    
}
