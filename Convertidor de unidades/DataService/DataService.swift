//
//  DataService.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 09/02/21.
//

import UIKit

struct DataService {
    
    static let instance = DataService()
    
    //Properties
    
    private let categories : [Unit] = [
        
        Unit(name: "Distancia", image: (UIImage(systemName: "ruler")?.withTintColor(.white, renderingMode: .alwaysOriginal))!),
        Unit(name: "Peso", image: (UIImage(systemName: "bag")?.withTintColor(.white, renderingMode: .alwaysOriginal))!),
        Unit(name: "Divisa", image: (UIImage(systemName: "dollarsign.circle")?.withTintColor(.white, renderingMode: .alwaysOriginal))!),
        Unit(name: "Cuota de apuesta", image: (UIImage(systemName: "sportscourt")?.withTintColor(.white, renderingMode: .alwaysOriginal))!),
        Unit(name: "Tiempo", image: (UIImage(systemName: "timer")?.withTintColor(.white, renderingMode: .alwaysOriginal))!),
        Unit(name: "Temperatura", image: (UIImage(systemName: "thermometer.sun")?.withTintColor(.white, renderingMode: .alwaysOriginal))!)
        
    ]
    
    //Methods
    
    private let lenghtUnits : [String] = ["Metros", "Yardas", "Pies", "Pulgadas", "Millas"]
    
    private let weigthUnits : [String] = ["Kilogramos", "Libras", "Onzas"]
    
    private let badgeUnits : [String] = ["$ Dólar", "$ Peso mexicano", "£ Libra", "€ Euro", "฿ Bitcoin"]
    
    private let betUnits : [String] = ["Cuota decimal 🇪🇺", "Cuota fraccionaria 🇬🇧", "Cuota americana 🇺🇸"]
    
    private let timeUnits : [String] = ["Horas", "Minutos", "Segundos"]
    
    private let temperatureUnits : [String] = ["Celcius (°C)", "Fahrenheit (°F)", "Kelvin (K)"]

    private let currencyDictionary : [String : String] = ["$ Dólar" : "USD", "$ Peso mexicano" : "MXN", "£ Libra" : "GBP", "€ Euro" : "EUR", "฿ Bitcoin" : "BTC"]
    
    func getCategories () -> [Unit] {
        
        return categories
        
    }
    
    func getLenghtUnits () -> [String] {
        
        return lenghtUnits
        
    }
    
    func getWeightUnits () -> [String] {
        
        return weigthUnits
        
    }
    
    func getBadgeUnits () -> [String] {
        
        return badgeUnits
        
    }
    
    func getBetUnits () -> [String] {
        
        return betUnits
        
    }
    
    func getTimeUnits () -> [String] {
        
        return timeUnits
        
    }
    
    func getTemperatureUnits () -> [String] {
        
        return temperatureUnits
        
    }
    
    func getCurrencyValue (key : String) -> String? {
        
        switch key {
        
        case "$ Dólar":
            
            return unwrappValue(key)
            
        case "$ Peso mexicano":
            
            return unwrappValue(key)
            
        case "£ Libra" :
            
            return unwrappValue(key)
            
        case "€ Euro" :
        
            return unwrappValue(key)
        
        case "฿ Bitcoin" :
            
            return unwrappValue(key)
            
        default:
            
            return nil
            
        }
        
    }
    
    private func unwrappValue (_ key : String) -> String? {
        
        
        if let currency = currencyDictionary[key] {
            
            return currency
            
        } else {
            
            return nil
            
        }
        
    }
    
    func getCoinTitle (value : String) -> String{
        
        switch value {
        
        case "BTC":
            
            return "฿ Bitcoin"
            
        case "USD" :
            
            return "$ Dólar"
            
        case "MXN" :
        
            return "$ Peso Mexicano"
            
        case "EUR" :
            
            return "€ Euro"
        
        case "GBP" :
        
            return "£ Libra"
        
        default:
            
            return ""
        }
        
        
        
    }
    
}

