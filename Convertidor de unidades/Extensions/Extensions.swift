//
//  Extensions.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 14/02/21.
//

import Foundation

extension Float {
    
    func round(to places : Int) -> Float {
        
        let precisicionNumber = pow(10, Float(places))
        var floatNumber = self
        floatNumber = floatNumber * precisicionNumber
        floatNumber.round()
        floatNumber = floatNumber / precisicionNumber
        return floatNumber
        
    }
        
}

extension Double {
    
    func round(to places : Int) -> Double {
        
        let precisicionNumber = pow(10, Double(places))
        var floatNumber = self
        floatNumber = floatNumber * precisicionNumber
        floatNumber.round()
        floatNumber = floatNumber / precisicionNumber
        return floatNumber
        
    }
    
}

