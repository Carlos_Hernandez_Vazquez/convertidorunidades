//
//  Protocols.swift
//  Convertidor de unidades
//
//  Created by mac on 19/02/21.
//

import Foundation

protocol  MenuControllerDelegate {
    
    func didSelectMenuItem(unit: Unit)
    
}

protocol CoinManagerDelegate {
    
    func didSelectOption (quote : String)
    
}
