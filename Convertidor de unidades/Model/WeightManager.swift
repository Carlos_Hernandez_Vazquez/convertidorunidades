//
//  WeightManaget.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 13/02/21.
//

import Foundation

class WeightManager : UnitManager {
    
    private var kilograms : Float?
    private var pounds : Float?
    private var ounces : Float?
    
    private var kilogramsToPounds : Float {
        
        return kilograms! * 2.205
        
    }
    
    private var kilogramsToOunces : Float {
        
        return kilograms! * 35.274
        
    }
    
    private var poundsToKilograms : Float {
        
        return pounds! / 2.205
        
    }
    
    private var poundsToOunces : Float {
        
        return pounds! * 16
        
    }
    
    private var ouncesToKilograms : Float {
        
        return ounces! / 35.274
        
    }
    
    private var ouncesToPounds : Float {
        
        return ounces! / 16
        
    }
    
    override func calculate(number: Float, operation: String) -> [String] {
        
        switch operation {
        
        case "Kilogramos":
            
            titles = super.getUnitTitles(titles: "Libras", "Onzas")
            
            kilograms = number
            
            return convertToStringArray(elements: [kilogramsToPounds, kilogramsToOunces])
            
        case "Libras":
            
            titles = super.getUnitTitles(titles: "Kilogramos", "Onzas")
            
            pounds = number
            
            return convertToStringArray(elements: [poundsToKilograms, poundsToOunces])
            
        default:
        
            titles = super.getUnitTitles(titles: "Kilogramos", "Libras")
            
            ounces = number
            
            return convertToStringArray(elements: [ouncesToKilograms, ouncesToPounds]) 
            
        }

        
    }
    
}
