//
//  TemperatureManager.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 12/02/21.
//

import Foundation


class TemperatureManager : UnitManager {
    
    private var celciusDegrees : Float?
    private var fahrenheitDegrees : Float?
    private var kelvinDegrees : Float?
    
    
    var celciusToFahrenheit : Float {
        
        get {
            
            return (celciusDegrees! * 1.8) + 32
            
        }
        
    }
    
    var celciusTokelvin : Float {
        
        get {
            
            return celciusDegrees! + 273.15
            
        }
        
    }
    
    var fahrenheitToCelcius : Float {
        
        return  (fahrenheitDegrees! - 32) * (5/9)
        
    }
    
    var fahrenheitToKelvin : Float {
        
        return (fahrenheitDegrees! + 459.67) * (5/9)
        
    }
    
    var kelvinToCelcius : Float {
        
        return (kelvinDegrees! - 273.15)
        
    }
    
    var kelvinToFahrenheit : Float {
        
        return (kelvinDegrees! - 273.15) * (9/5) + 32
        
    }
    
    override func calculate(number: Float, operation: String) -> [String] {
        
        switch operation {
        
        case "Celcius (°C)":
            
            titles = super.getUnitTitles(titles: "Fahrenheit (°F)", "Kelvin (K)")
            
            celciusDegrees = number
            
            return convertToStringArray(elements: [celciusToFahrenheit, celciusTokelvin])
            
        case "Fahrenheit (°F)":
            
            titles = super.getUnitTitles(titles: "Celcius (°C)", "Kelvin (K)")
            
            fahrenheitDegrees = number
            
            return  convertToStringArray(elements: [fahrenheitToCelcius, fahrenheitToKelvin])
            
        default:
        
            titles = super.getUnitTitles(titles: "Celcius (°C)", "Fahrenheit (°F)")
            
            kelvinDegrees = number
            
            return  convertToStringArray(elements: [kelvinToCelcius, kelvinToFahrenheit]) 
            
        }
        
    }
    
}







