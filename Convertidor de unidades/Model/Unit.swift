//
//  Category.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 09/02/21.
//

import UIKit

struct Unit {
    
    private (set) public var name : String
    private (set) public var image : UIImage
    
    init(name : String, image : UIImage) {
        
        self.name = name
        self.image = image
        
    }
        
}
