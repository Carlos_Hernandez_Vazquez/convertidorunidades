//
//  BetManager.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 25/02/21.
//

import Foundation

public class BetManager : UnitManager {

    public func calculateBet (amountToBet : Double, quota : String, denominator : Float?, option : String) -> (results : [String]?, profit : Float) {
        
        var resultsArray : [String] = []
        
        switch option {
        
        case "Cuota decimal 🇪🇺" :
            
            let decimalQuota = Double(quota)!
            
            titles = getUnitTitles(titles: "Cuota fraccionaria 🇬🇧", "Cuota americana 🇺🇸")
            
            resultsArray.append(getFraction(with: decimalQuota))
            resultsArray.append(convertDecimalToAmerican(with: decimalQuota))
            
            return (resultsArray, calculateDecimal(number: amountToBet, with: decimalQuota))
            
        case "Cuota fraccionaria 🇬🇧" :
            
            titles = getUnitTitles(titles: "Cuota decimal 🇪🇺", "Cuota americana 🇺🇸")
            
            if let denominator = denominator {
                
                let numerator = Float(quota)!
                let decimalNumber = convertFractionToDecimal(numerator: numerator, denominator: denominator)
                
                resultsArray.append(convertFractionalToDecimal(numerator: numerator, denominator: denominator))
                resultsArray.append(convertDecimalToAmerican(with: Double(decimalNumber)))
                
                return (resultsArray, calculateFractional(number: amountToBet, numerator: (numerator), denominator: (denominator)))
                
            }
            
        case "Cuota americana 🇺🇸":
            
            titles = getUnitTitles(titles: "Cuota decimal 🇪🇺", "Cuota fraccionaria 🇬🇧")
            
            resultsArray.append(convertAmericanToDecimal(with: quota))
            resultsArray.append(convertAmericanToFractional(with: quota))
            
            return (resultsArray, calculateAmerican(number: amountToBet, with: quota))
            
        default :
        
            break
        
        }
        
        return (["Sin resultados"], 0)
    }
    
    //rationalApproximationOf was created by Martin R
    
    private func rationalApproximationOf(x0 : Double, withPrecision eps : Double = 1.0E-6) -> (num : Int, dom : Int) {
        var x = x0
        var a = floor(x)
        var (h1, k1, h, k) = (1, 0, Int(a), 1)

        while x - a > eps * Double(k) * Double(k) {
            x = 1.0/(x - a)
            a = floor(x)
            (h1, k1, h, k) = (h, k, h1 + Int(a) * h, k1 + Int(a) * k)
        }
        return (h, k)
    }

    private func calculateDecimal (number : Double, with quota : Double ) -> Float {
        
        return Float(number * quota)
        
    }
    
    private func calculateFractional (number : Double, numerator : Float, denominator : Float) -> Float {
        
        if numerator > denominator {
            
            let profit = ((numerator) / (denominator)) + 1
            
            return Float(number) * profit
            
        } else {
            
            let profit = Float(number) * ((numerator) / (denominator))
            print(profit)
            
            return Float(number) + profit
            
        }
        
    }

    
    private func calculateAmerican (number : Double, with quota : String) -> Float {
        
        var americanNumber = quota
        let americanNumberFloat : Float
        
        if quota.first == "+" {
            
            americanNumber.removeFirst()
            americanNumberFloat = Float(americanNumber)!
            
            return ((americanNumberFloat / 100 + 1) * Float(number).round(to: 3))
            
            
        } else {
            
            americanNumber.removeFirst()
            americanNumberFloat = Float(americanNumber)!
            
            return ((100 / americanNumberFloat + 1) * Float(number).round(to: 3))

        }
        
    }
    
    private func getFraction (with quota : Double) -> String {
        
        let decimalToFractional  = rationalApproximationOf(x0: (quota - 1))
        
        let fraction : String = "\(decimalToFractional.num)/\(decimalToFractional.dom)"
        
        return fraction
        
    }
    
    private func convertAmericanToDecimal (with americanQuota : String) -> String{
        
        var americanQuotaNumber = americanQuota
        americanQuotaNumber.removeFirst()
        let americanQuotaNumberInteger : Int = Int(americanQuotaNumber)!
        
        if americanQuota.first == "+" {
            
            return String(convertAmericanToDecimalPositive(number: americanQuotaNumberInteger))
            
        } else {
            
            return String(convertToDecimalNegative(number: americanQuotaNumberInteger))
            
        }
        
    }
    
    private func convertAmericanToFractional (with americanQuota : String) -> String {
        
        var americanQuotaNumber = americanQuota
        americanQuotaNumber.removeFirst()
        let americanQuotaNumberDouble = Double(americanQuotaNumber)!
        
        if americanQuota.first == "+" {
            
            let fractionPositive = rationalApproximationOf(x0: Double(convertAmericanToDecimalPositive(number: Int(americanQuotaNumberDouble)) - 1 ))
            
            return String("\(fractionPositive.num)/\(fractionPositive.dom)")
            
        } else {
            
            let fractionNegative = rationalApproximationOf(x0: Double(convertToDecimalNegative(number: Int(americanQuotaNumberDouble)) - 1))
            
            return String("\(fractionNegative.num)/\(fractionNegative.dom)")
            
        }
            
    }
    
    private func convertAmericanToDecimalPositive (number : Int) -> Float {
        
        return (Float(number) / 100) + 1
        
    }
    
    public func convertToDecimalNegative (number : Int) -> Float {
        
        return (100 / Float(number) + 1).round(to: 3)
        
    }
    
    private func convertFractionToDecimal (numerator : Float, denominator : Float) -> Float{
        
        return ((numerator / denominator) + 1)
        
    }
    
    private func convertFractionalToDecimal (numerator : Float , denominator : Float) -> String {
        
        return String(((numerator / denominator) + 1).round(to: 3))
        
    }
    
    private func convertDecimalToAmerican (with quota : Double) -> String {
        
        if quota >= 2.0 {
            
            return convertDecimalToAmericanPositive(quota)
            
        } else {
            
            return convertDecimalToAmericanNegative(quota)
            
        }
        
    }
    
    private func convertDecimalToAmericanPositive (_ decimalNumber : Double) -> String {
        
        return String("+\((decimalNumber * Double(100) - 100))".dropLast(2))
        
    }
    
    private func convertDecimalToAmericanNegative (_ decimalNumber : Double) -> String {
        
        var conversion : Double = 100 / (decimalNumber - 1)
        conversion.round()
        
        return String("-\(conversion)".dropLast(2))
        
    }
    
}
