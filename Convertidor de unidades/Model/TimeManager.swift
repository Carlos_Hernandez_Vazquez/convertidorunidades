//
//  TimeManager.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 13/02/21.
//

import Foundation

class TimeManager : UnitManager {
    
    private var hours : Float?
    private var minutes : Float?
    private var seconds : Float?
    
    private var secondsToMinutes : Float {
        
        return seconds! / 60
        
    }
    
    private var secondsToHours : Float {
        
        return (seconds! / 3600).round(to: 2)
        
    }
    
    private var minutesToSeconds : Float {
        
        return minutes! * 60
        
    }
    
    private var minutesToHours : Float {
        
        return (minutes! / 60).round(to: 2)
        
    }
    
    private var hoursToSeconds : Float {
        
        return (hours! * 3600)
        
    }
    
    private var hoursToMinutes : Float {
        
        return (hours! * 60)
        
    }
    
    override func calculate(number: Float, operation: String) -> [String] {
        
        switch operation {
        
        case "Horas":
            
            titles = super.getUnitTitles(titles: "Minutos", "Segundos")
            
            hours = number
            
            return convertToStringArray(elements: [hoursToMinutes, hoursToSeconds])
            
        case "Minutos":
            
            titles = super.getUnitTitles(titles: "Segundos", "Horas")
            
            minutes = number
            
            return convertToStringArray(elements: [minutesToSeconds, minutesToHours])
            
        default:
        
            titles = super.getUnitTitles(titles: "Minutos", "Horas")
            
            seconds = number
            
            return convertToStringArray(elements: [secondsToMinutes, secondsToHours]) 
            
        }

    }
    
}
