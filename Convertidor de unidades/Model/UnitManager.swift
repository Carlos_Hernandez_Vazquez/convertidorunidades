//
//  UnitManager.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 15/02/21.
//

import Foundation

public class UnitManager {
    
    public var titles = [String]()
    
    func calculate (number : Float, operation : String) -> [String] {
        return [String]()
    }
    
    func getUnitTitles(titles : String...) -> [String] {
        
        var titleArray = [String]()
        
        titleArray.append(contentsOf: titles)
        
        return titleArray
        
    }
    
    public func convertToStringArray (elements : [Float]) -> [String] {
        
        return elements.map { ("\($0)")
            
        }
        
    }
    
}
