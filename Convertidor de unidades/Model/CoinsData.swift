//
//  CoinData.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 17/02/21.
//

import Foundation

struct CoinsData : Codable {
    
    let asset_id_base : String
    let rates : [CoinData]
    
}

struct CoinData : Codable {
    
    let asset_id_quote : String
    let rate : Double
    
}

