//
//  DistanceManager.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 15/02/21.
//

import Foundation

class DistanceManager : UnitManager  {
    
    private var meters : Float?
    private var yards : Float?
    private var feet : Float?
    private var inches : Float?
    private var miles : Float?
    
    override func calculate(number: Float, operation: String) -> [String] {
        
        switch operation {
        
        case "Metros":
            print("Calculando Metros")
            meters = number
            return convertToStringArray(elements: calculateMeters(meters: meters!))
            
        case "Yardas" :
            
            yards = number
            return convertToStringArray(elements: calculateYards(yards: yards!))
            
        case "Pies" :
            
            feet = number
            return convertToStringArray(elements: calculateFeet(feet: feet!))
            
        case "Pulgadas" :
            
            inches = number
            return convertToStringArray(elements: calculateInches(inches: inches!))
            
        case "Millas" :
            
            miles = number
            return convertToStringArray(elements: calculateMiles(miles: miles!))
            
            
        default:
            
            break
            
        }

        return [""]
        
    }
    
    private func calculateMeters (meters : Float) -> [Float] {
        
        titles = super.getUnitTitles(titles: "Yardas", "Pies", "Pulgadas", "Millas")
        
        let metersToYards = meters / 1.094
        let metersToFeet = meters * 3.281
        let metersToInches = meters * 39.37
        let metersToMiles = meters / 1609
        
        return [metersToYards, metersToFeet, metersToInches, metersToMiles]
        
    }
    
    private func calculateYards (yards : Float) -> [Float] {
        
        titles = super.getUnitTitles(titles: "Metros", "Pies", "Pulgadas", "Millas")
        
        let yardsToMeters = yards / 1.094
        let yardsToFeet = yards * 3
        let yardsToInches = yards * 36
        let yardsToMiles = yards / 1760
        
        return [yardsToMeters, yardsToFeet, yardsToInches, yardsToMiles]
        
    }
    
    private func calculateFeet (feet : Float) -> [Float] {
        
        titles = super.getUnitTitles(titles: "Metros", "Yardas", "Pulgadas", "Millas")
        
        let feetToMeters = feet / 3.281
        let feetToYards = feet / 3
        let feetToIches = feet * 12
        let feetToMiles = feet / 5280
        
        return [feetToMeters, feetToYards, feetToIches, feetToMiles]
        
    }
    
    private func calculateInches (inches : Float) -> [Float] {
        
        titles = super.getUnitTitles(titles: "Metros", "Yardas", "Pies", "Millas")
        
        let inchesToMeters = inches / 39.7
        let inchesToYards = inches / 36
        let inchesToFeet = inches / 12
        let inchesToMiles = inches / 63360
        
        return [inchesToMeters, inchesToYards, inchesToFeet, inchesToMiles]
        
    }
    
    private func calculateMiles (miles : Float) -> [Float] {
        
        titles = super.getUnitTitles(titles: "Metros", "Yardas", "Pies", "Pulgadas")
        
        let milesToMeters = miles * 1609
        let milesToYards = miles * 1760
        let milesToFeet = miles * 5280
        let milesToInches = miles * 63360
        
        return [milesToMeters, milesToYards, milesToFeet, milesToInches]
        
    }

}
