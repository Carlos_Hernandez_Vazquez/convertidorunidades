//
//  CoinManager.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 17/02/21.
//

import Foundation

class CoinManager : UnitManager {
    
    private let BASE_URL = "https://rest.coinapi.io/v1/exchangerate/"
    private let apiKey = key
    private let URL_FILTER = "&filter_asset_id="
    private let URL_SESSION  = URLSession(configuration: .default)
    public var baseQuote : String?
    public var number : Float?
    
    var delegate : CoinManagerDelegate?
    
    func getBaseQuote (key : String) {
        
        baseQuote = key
        delegate?.didSelectOption(quote: baseQuote!)
        
    }
    
     func performRequest (with filter : String, onSucces : @escaping onSuccess, onError : @escaping onError) {
        
        let url = URL(string: "\(BASE_URL)\(baseQuote!)?apikey=\(apiKey)\(URL_FILTER)\(filter)")!
        
        let task = URL_SESSION.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    onError(error.localizedDescription)
                    
                    return
                    
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    
                    onError("No se pudo obtener respuesta del servidor")
                    
                    return
                    
                }
                
                
                do{
                    
                    if response.statusCode == 200 {
                        
                        let coins = try JSONDecoder().decode(CoinsData.self, from: data)

                        onSucces(coins)
                        
                    } else {
                        
                        switch response.statusCode {
                        
                        case 400:
                            
                            onError("Error \(response.statusCode). There is something wrong with your request")
                            
                        case 401 :
                            
                            onError("Error \(response.statusCode). Your API key is wrong")
                            
                        case 403 :
                            
                            onError("Error \(response.statusCode). Your API key doesnt't have enough privileges to access this resource")
                            
                        case 429 :
                            
                            onError("Error \(response.statusCode). You have exceeded your API key rate limits")
                            
                        case 550 :
                            
                            onError("Error \(response.statusCode). You requested specific single item that we don't have at this moment.")
                            
                        default:
                            onError("Error \(response.statusCode)")
                        }
                        
                        
                    }
                    
                    
                } catch {
                    
                    onError(error.localizedDescription)
                    
                }
                
                
            }
            
        }
 
        task.resume()
        
    }
        
        
    func getCoinsWithFilter (firstCoin : String, secondCoin : String, thirdCoin : String, fourthCoin : String) -> String{
            
        return "\(firstCoin),\(secondCoin),\(thirdCoin),\(fourthCoin)"
            
    }

    func convert (coinRate : Double) -> Double {
        
        return coinRate * Double(number!)
        
    }
    
}
