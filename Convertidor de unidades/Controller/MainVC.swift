//
//  ViewController.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández on 09/02/21.
//

import UIKit
import SideMenu

class MainVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var noSelectionLabel: UILabel!
    @IBOutlet weak var convertButton: UIButton!
    @IBOutlet weak var unitImage: UIBarButtonItem!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var betStack: UIStackView!
    @IBOutlet weak var betSegmentedControl: UISegmentedControl!
    @IBOutlet weak var decimalBetStack: UIStackView!
    @IBOutlet weak var fractionalStack: UIStackView!
    @IBOutlet weak var decimalBetTextField: UITextField!
    @IBOutlet weak var numeratorTextField: UITextField!
    @IBOutlet weak var denominatorTextField: UITextField!
    
    
    //MARK: - Properties
    
    private var sideMenu : SideMenuNavigationController?
    private var measurementUnits : [String] = []
    private var unitName : String = ""
    private var numberToConvert : Float = 0.0
    private var temperatureManager = TemperatureManager()
    private var timeManager = TimeManager()
    private var weightManager = WeightManager()
    private var distanceManager = DistanceManager()
    private var unitConversion : UnitConversion?
    private var betManager = BetManager()
    private var americanQuote : String?
    
    //MARK: - Lyfe Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let menu = MainListTableViewController()
        menu.delegate = self
        pickerView.delegate = self
        amountTextField.delegate = self
        configureMenu(menu)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideOutlets()
    }
    
    
    //MARK: - Actions
    
    @IBAction func menuButtonWasPressed(_ sender: UIBarButtonItem) {
        
        guard let menu = sideMenu else { return }
        
        present(menu, animated: true)
        
        
    }
    
    @IBAction func convertButtonWasPressed(_ sender: UIButton) {
        
        if let text = amountTextField.text {
            
            if !text.isEmpty {
                
                if isValidDecimalNumber(number: text) {
                    
                    amountTextField.text = setZeroAtFirst(text: text)
                    numberToConvert = Float(setZeroAtFirst(text: text)) ?? 0.0
                    
                    guard let unit = unitConversion else { return }
                    
                    if (unit == .temperature) || (unit == .time) || (unit == .weight){
                        
                        performSegue(withIdentifier: "toConvertThreeUnits", sender: self)
                        
                    } else if (unit == .distance || unit == .badge) {
                        
                        performSegue(withIdentifier: "toConvertFiveUnits", sender: self)
                        
                    } else if (unit == .bet) {
                        
                        guard let decimal = decimalBetTextField.text else { return }
                        
                        if (unitName == "Cuota decimal 🇪🇺") {
                            
                            let decimalQuota = Float(decimal) ?? 0.0
                            
                            if decimalQuota < 1.0 {
                                
                                addAlert(message: "Su cuota debe ser mayor a 1.0")
                                
                            } else if !isValidDecimalNumber(number: decimal) {
                                
                                addAlert(message: "El número ingresado es incorrecto.")
                                
                            } else if !decimal.contains(".") {
                                
                                addAlert(message: "Agregue . a su cuota")
                                
                            } else {
                                
                                performSegue(withIdentifier: "toConvertThreeUnits", sender: self)
                                
                            }
                            
                        } else if (unitName == "Cuota americana 🇺🇸") {
                            
                            let americanNumber = Float(decimal) ?? 0
                            
                            if americanNumber == 0 {
                                
                                addAlert(message: "Cuota incorrecta")
                                
                            } else {
                                
                                performSegue(withIdentifier: "toConvertThreeUnits", sender: self)
                                
                            }
                            
                        } else if (unitName == "Cuota fraccionaria 🇬🇧") {
                            
                            guard let numerator = numeratorTextField.text, let denominator = denominatorTextField.text else { return }
                         
                            if numerator == "" || denominator == "" {
                                
                                addAlert(message: "Llene todos los campos.")
                                
                            } else {
                                
                                performSegue(withIdentifier: "toConvertThreeUnits", sender: self)
                                
                            }
                            
                        }
                        
                    }

                } else {
                    
                    let alert = UIAlertController(title: "Número inválido", message: "El número ingresado es incorrecto. Vuelve a intentarlo", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    alert.addAction(action)
                    
                    present(alert, animated: true, completion: nil)
                    
                }
                
            } else {
                
                let alert = UIAlertController(title: "Error", message: "No has ingresado niguna cantidad", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(action)
                
                present(alert, animated: true, completion: nil)
                
            }

            
        }
        
                
    }
    
    @IBAction func betSegmentedActionWasChanged(_ sender: UISegmentedControl) {
        
        guard let americanQuotaText = decimalBetTextField else { return }
        
        switch betSegmentedControl.selectedSegmentIndex {
        
        case 0:
            
            americanQuote = "+\(americanQuotaText)"
            
        case 1:
        
            americanQuote = "-\(americanQuotaText)"
            
        default:
            break
        }
        
    }
    
    
    
    //MARK: - Methods
    
    private func configureMenu (_ menu : MainListTableViewController) {
        
        sideMenu = SideMenuNavigationController(rootViewController: menu)
        sideMenu?.leftSide = true
        sideMenu?.setNavigationBarHidden(true, animated: true)
        SideMenuManager.default.leftMenuNavigationController = sideMenu
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        
    }
    
    private func updateViewForConvertion (unit : Unit) {
        
        noSelectionLabel.text = "Seleccione una opción"
        noSelectionLabel.font = UIFont(name: "Avenir Next Medium", size: 17)!
        noSelectionLabel.tintColor = .black
        self.title = unit.name
        self.unitImage.image = unit.image.withTintColor(.black, renderingMode: .alwaysOriginal)
        
        switch unit.name {
        
        case "Distancia":
            
            measurementUnits = DataService.instance.getLenghtUnits()
            unitConversion = .distance
            showOutlets()
            hideBetView()
            reloadView()
            
        case "Peso" :
            
            unitConversion = .weight
            measurementUnits = DataService.instance.getWeightUnits()
            showOutlets()
            hideBetView()
            reloadView()
        
        case "Divisa" :
            
            unitConversion = .badge
            measurementUnits = DataService.instance.getBadgeUnits()
            hideBetView()
            showOutlets()
            reloadView()
            
        case "Cuota de apuesta" :
            
            unitConversion = .bet
            measurementUnits = DataService.instance.getBetUnits()
            showOutlets()
            reloadView()

        case "Tiempo" :
            
            unitConversion = .time
            measurementUnits = DataService.instance.getTimeUnits()
            hideBetView()
            showOutlets()
            reloadView()
            
        case "Temperatura" :
            
            unitConversion = .temperature
            measurementUnits = DataService.instance.getTemperatureUnits()
            hideBetView()
            showOutlets()
            reloadView()
        
        default:
            break
        }
        
    }
    
    private func hideOutlets () {
        
        convertButton.isHidden = true
        pickerView.isHidden = true
        amountLabel.isHidden = true
        amountTextField.isHidden = true
        betStack.isHidden = true
        
    }
    
    private func hideBetView () {
        
        betStack.isHidden = true
        
    }
    
    private func showOutlets () {
        
        if unitConversion! == .bet {
            
            betStack.isHidden = false
            decimalBetStack.isHidden = false
            fractionalStack.isHidden = true
            betSegmentedControl.isHidden = true
            convertButton.isHidden = false
            convertButton.backgroundColor = #colorLiteral(red: 0.3143242386, green: 0.3143242386, blue: 0.3143242386, alpha: 1)
            pickerView.isHidden = false
            convertButton.isEnabled = false
            amountTextField.isHidden = false
            amountLabel.isHidden = false
            
            
        } else {
            
            convertButton.isHidden = false
            convertButton.backgroundColor = #colorLiteral(red: 0.3143242386, green: 0.3143242386, blue: 0.3143242386, alpha: 1)
            pickerView.isHidden = false
            convertButton.isEnabled = false
            amountTextField.isHidden = false
            amountLabel.isHidden = false
            
        }
        
    }
    
    private func activeButton () {
        
        if unitConversion! == .bet {
            
            guard let decimalBetTextField = decimalBetTextField.text else { return }
            
            
            switch unitName {
            
            case "Cuota decimal 🇪🇺" :
                
                
                if (checkAmountTextField()) && (!decimalBetTextField.isEmpty) {
                    
                    convertButton.isEnabled = true
                    convertButton.backgroundColor = .black
                    
                }
                
            case  "Cuota americana 🇺🇸" :
                
                if (checkAmountTextField()) && (!decimalBetTextField.isEmpty) {
                    
                    convertButton.isEnabled = true
                    convertButton.backgroundColor = .black
                    
                }
                
            case "Cuota fraccionaria 🇬🇧" :
                
                guard let numerator = numeratorTextField.text, let denominator = denominatorTextField.text else { return }
                
                if (checkAmountTextField()) && (!numerator.isEmpty) && (!denominator.isEmpty) {
                    
                    convertButton.isEnabled = true
                    convertButton.backgroundColor = .black
                    
                }
                
                
            default:
                
                fatalError()
                break
                
            }
            
        } else {
            
            convertButton.isEnabled = true
            convertButton.backgroundColor = .black
            
        }
                        
    }
    
    private func addAlert (message : String) {
        
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        
    }
    
    private func checkAmountTextField () -> Bool {
        
        guard let amountTextField = amountTextField.text else { return false}
        
        if !amountTextField.isEmpty {
        
            return true

        } else {
            
            return false
        }
        
    }
    
    private func reloadView () {
        
        amountTextField.text = ""
        convertButton.isEnabled = false
        pickerView.selectRow(0, inComponent: 0, animated: false)
        unitName = measurementUnits[0]
        pickerView.reloadAllComponents()
        
    }
    
    private func reloadViewToBetConversion () {
        
        amountTextField.text = ""
        convertButton.isEnabled = false
        pickerView.selectRow(0, inComponent: 0, animated: false)
        unitName = measurementUnits[0]
        
        pickerView.reloadAllComponents()
        
    }
    
    func isValidDecimalNumber (number : String) -> Bool {
        
        var decimalPoint = 0
        
        for digit in number {
            
            if digit == "." {
                
                decimalPoint += 1
                
            }
            
        }
        
        return decimalPoint <= 1
        
    }
    
    func setZeroAtFirst (text : String) -> String {
        
        var numberWithZero : String
        
        if text.first == "." {
            
            numberWithZero = text
            numberWithZero.insert("0", at: numberWithZero.startIndex)
            return numberWithZero
            
        } else {
            
            return text
            
        }
        
    }
    
    private func emptyTextFields () {
        
        numeratorTextField.text = ""
        denominatorTextField.text = ""
        decimalBetTextField.text = ""
        
    }
    
    func initValuesToNextViewController (viewController : ConversionVC, number : Float, results : [String]?, titles : [String]) {
        
        viewController.convertedUnitName = self.unitName
        viewController.convertedUnitNumber = number
        viewController.unitNames = titles
        viewController.results = results ?? [""]
        
    }

    func initValuesToNextViewController (viewControllerWithFiveUnits : ConvertFiveUnitsVC, results : [String]?, titles : [String]?, coinConversion : Bool) {
        
        viewControllerWithFiveUnits.convertedUnitName = self.unitName
        viewControllerWithFiveUnits.convertedUnitNumber = self.numberToConvert
        viewControllerWithFiveUnits.unitNames = titles ?? ["No Names"]
        viewControllerWithFiveUnits.results = results ?? [""]
        viewControllerWithFiveUnits.isCoinConversion = coinConversion
        
    }
    
    private func getAmericanQuotaBySegmentedControlIndex (index : Int) -> String {
        
        if let americanNumber = decimalBetTextField.text {
            
            if index == 0 {
                
                return "+\(americanNumber)"
                
            } else {
                
                return "-\(americanNumber)"
                
            }
            
        }
        
        return ""
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if  unitName != "" {
                
            self.view.endEditing(true)
            activeButton()
            
        } else {
            
            debugPrint("Touches Began")
            
        }
            
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let unit = unitConversion else { return }
        
        if segue.identifier == "toConvertThreeUnits"  {
            
            guard let conversionVC = segue.destination as? ConversionVC else { return }
            
            switch unit {
            
            case .temperature:
            
                initValuesToNextViewController(viewController: conversionVC, number: numberToConvert, results: temperatureManager.calculate(number: numberToConvert, operation: unitName), titles: temperatureManager.titles)


            case .time:
                
                initValuesToNextViewController(viewController: conversionVC, number: numberToConvert, results: timeManager.calculate(number: numberToConvert, operation: unitName), titles: timeManager.titles)
                
                
            case .weight :
                            
                initValuesToNextViewController(viewController: conversionVC, number: numberToConvert, results: weightManager.calculate(number: numberToConvert, operation: unitName), titles: weightManager.titles)
                
            case .bet :
                
                conversionVC.isBetConversion = true
                
                switch unitName {
                
                
                case "Cuota decimal 🇪🇺" :
                
                    let tuple = betManager.calculateBet(amountToBet: Double(numberToConvert), quota: decimalBetTextField.text!, denominator: nil, option: unitName)
                    
                    initValuesToNextViewController(viewController: conversionVC, number: numberToConvert, results: tuple.results, titles: betManager.titles)
                    conversionVC.profit = tuple.profit
                     
                    
                case "Cuota fraccionaria 🇬🇧" :
                    
                    if let numerator = numeratorTextField.text, let denominator = denominatorTextField.text {
                        
                        let tuple = betManager.calculateBet(amountToBet: Double(numberToConvert), quota: numerator, denominator: Float(denominator), option: unitName)
                        
                        
                        initValuesToNextViewController(viewController: conversionVC, number: numberToConvert, results: tuple.results, titles: betManager.titles)
                        
                        conversionVC.profit = tuple.profit

                        
                    }
                    
                                        
                case "Cuota americana 🇺🇸" :
                    
                    let americanNumber = getAmericanQuotaBySegmentedControlIndex(index: betSegmentedControl.selectedSegmentIndex)
                    
                    let tuple = betManager.calculateBet(amountToBet: Double(numberToConvert), quota: americanNumber, denominator: nil, option: unitName)
                    
                    initValuesToNextViewController(viewController: conversionVC, number: numberToConvert, results: tuple.results, titles: betManager.titles)
                    
                    conversionVC.profit = tuple.profit
                    
                default:
                    
                    break
                }
                
            default : 
            
                debugPrint("No hay selección")
                break
                
            }
            
        } else if segue.identifier == "toConvertFiveUnits" {
            
            guard let conversionFiveUnits = segue.destination as? ConvertFiveUnitsVC else { return }
            
            if unit == .distance {
            
                initValuesToNextViewController(viewControllerWithFiveUnits: conversionFiveUnits, results: distanceManager.calculate(number: numberToConvert, operation: unitName), titles: distanceManager.titles, coinConversion: false)
                
            } else if unit == .badge {
                
                print("Conversion")
                initValuesToNextViewController(viewControllerWithFiveUnits: conversionFiveUnits, results: nil, titles: nil, coinConversion: true)
                
            }
            
        }
        
    }
    
}

//MARK: - MenuControllerDelegate Methods

extension MainVC : MenuControllerDelegate {
    
    func didSelectMenuItem(unit: Unit) {
      
        updateViewForConvertion(unit: unit)
        
    }
    
}

extension MainVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return measurementUnits.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return measurementUnits[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        unitName = measurementUnits[row]
        
        if unitConversion! == .bet {
            
            if measurementUnits[row] == "Cuota decimal 🇪🇺" {
                
                convertButton.backgroundColor = #colorLiteral(red: 0.3143242386, green: 0.3143242386, blue: 0.3143242386, alpha: 1)
                convertButton.isEnabled = false
                betStack.isHidden = false
                decimalBetStack.isHidden = false
                betSegmentedControl.isHidden = true
                decimalBetTextField.isHidden = false
                fractionalStack.isHidden = true
                decimalBetTextField.keyboardType = .decimalPad
                emptyTextFields()
                
                
            } else if measurementUnits[row] == "Cuota fraccionaria 🇬🇧" {
                
                convertButton.backgroundColor = #colorLiteral(red: 0.3143242386, green: 0.3143242386, blue: 0.3143242386, alpha: 1)
                convertButton.isEnabled = false
                betStack.isHidden = false
                fractionalStack.isHidden = false
                decimalBetStack.isHidden = true
                numeratorTextField.textAlignment = .center
                denominatorTextField.textAlignment = .center
                emptyTextFields()
                
            } else {
                
                convertButton.backgroundColor = #colorLiteral(red: 0.3143242386, green: 0.3143242386, blue: 0.3143242386, alpha: 1)
                convertButton.isEnabled = false
                betStack.isHidden = false
                decimalBetStack.isHidden = false
                betSegmentedControl.isHidden = false
                fractionalStack.isHidden = true
                decimalBetTextField.keyboardType = .numberPad
                emptyTextFields()
                
            }
            
        }
        
    }
    
}

//MARK: - UITextFieldDelegate Methods

extension MainVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
}

