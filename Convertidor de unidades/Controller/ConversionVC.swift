//
//  TemperatureVC.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 12/02/21.
//

import UIKit

class ConversionVC : UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak private var firstUnitLabel: UILabel!
    @IBOutlet weak private var firstUnitResultLabel: UILabel!
    @IBOutlet weak private var secondUnitLabel: UILabel!
    @IBOutlet weak private var secondUnitResultLabel: UILabel!
    @IBOutlet weak private var thirdUnitLabel: UILabel!
    @IBOutlet weak private var thirdResultLabel: UILabel!
    
    //MARK: - Properties
    
    public var convertedUnitName : String?
    public var convertedUnitNumber : Float?
    public var unitNames = [String]()
    public var results = [String]()
    public var isBetConversion : Bool = false
    public var profit : Float?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkBetConversion()
    }
    
    private func checkBetConversion () {
        
        if isBetConversion {
            
            convertBet()
            
        } else {
            
            initView()
        }
        
    }
    
    func initView() {
        
        self.navigationItem.hidesBackButton = true
        
        if let unitName = convertedUnitName, let unitNumber = convertedUnitNumber {
            
            self.title = "Convertir \(unitName)"
            firstUnitLabel.text = unitName
            firstUnitResultLabel.text = String(describing: unitNumber)
            secondUnitLabel.text = unitNames[0]
            secondUnitResultLabel.text = results[0]
            thirdUnitLabel.text = unitNames[1]
            thirdResultLabel.text = results[1]
            
        }
        
    }
    
    private func convertBet () {
        
        guard let profit = profit, let unitName = convertedUnitName, let unitNumber = convertedUnitNumber else { return }
        
        self.title = "Apuesta"
        self.navigationItem.hidesBackButton = true
        
        print(results)
        firstUnitLabel.text = unitName
        firstUnitResultLabel.text = String("Apostando $\(unitNumber) ganarías $\(profit) pesos")
        secondUnitLabel.text = unitNames[0]
        secondUnitResultLabel.text = results[0]
        thirdUnitLabel.text = unitNames[1]
        thirdResultLabel.text = results[1]
        
    }
    
    @IBAction func homeButtonWasPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
