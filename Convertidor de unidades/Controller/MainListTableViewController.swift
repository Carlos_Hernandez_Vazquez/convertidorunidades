//
//  MainListTableViewController.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 09/02/21.
//

import UIKit

class MainListTableViewController: UITableViewController {

    private var categories = [Unit]()
    public var delegate : MenuControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        categories = DataService.instance.getCategories() 
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .white
    
        
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewForHeader = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        let menuLabel = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.width, height: 20))
        menuLabel.textColor = .white
        menuLabel.font = .boldSystemFont(ofSize: 25)
        menuLabel.text = "Convertir"
        viewForHeader.backgroundColor = .black
        viewForHeader.addSubview(menuLabel)
        
        return viewForHeader
        
    }
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return DataService.instance.getCategories().count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let category = categories[indexPath.row]
        
        cell.textLabel?.font = .boldSystemFont(ofSize: 17)
        cell.textLabel?.text = category.name
        cell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.textLabel?.textColor = .white
        cell.imageView?.image = category.image
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let category = categories[indexPath.row]
        delegate?.didSelectMenuItem(unit: category)
        
    }

}
 


