//
//  ConvertFiveUnitsVC.swift
//  Convertidor de unidades
//
//  Created by Carlos Hernández Vázquez on 15/02/21.
//

import UIKit

class ConvertFiveUnitsVC: ConversionVC {

    //MARK: - Outlets
    @IBOutlet weak var unitToConvertTitleLabel: UILabel!
    @IBOutlet weak var unitToConvertTResultLabel: UILabel!
    @IBOutlet weak var secondUnitTitleLabel: UILabel!
    @IBOutlet weak var secondUnitTResultLabel: UILabel!
    @IBOutlet weak var thirdUnitTitleLabel: UILabel!
    @IBOutlet weak var thirdUnitResultLabel: UILabel!
    @IBOutlet weak var fourthUnitTitleLabel: UILabel!
    @IBOutlet weak var fourthUnitResultLabel: UILabel!
    @IBOutlet weak var fifthUnitTitleLabel: UILabel!
    @IBOutlet weak var fifthUnitResultLabel: UILabel!
    @IBOutlet weak var updateTimeLabel: UILabel!
    
    
    //MARK: - Properties
    
    public var isCoinConversion = false
    private let coinManager = CoinManager()
    
    //MARK: - Lyfe Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkNetworkService()
    }
    
    //MARK: - ConversionFiveUnitsVC Methods
    
    override func initView() {
        
        self.navigationItem.hidesBackButton = true
        
        if let unitName = convertedUnitName, let unitNumber = convertedUnitNumber {
            
            self.title = "Convertir \(unitName)"
            unitToConvertTitleLabel.text = unitName
            unitToConvertTResultLabel.text = String(unitNumber)
            secondUnitTitleLabel.text = unitNames[0]
            secondUnitTResultLabel.text = results[0]
            thirdUnitTitleLabel.text = unitNames[1]
            thirdUnitResultLabel.text = results[1]
            fourthUnitTitleLabel.text = unitNames[2]
            fourthUnitResultLabel.text = results[2]
            fifthUnitTitleLabel.text = unitNames[3]
            fifthUnitResultLabel.text = results[3]
            
        }
        
    }
    
    func checkNetworkService () {
        
        if self.isCoinConversion {
            
            coinManager.number = convertedUnitNumber
            coinManager.getBaseQuote(key: convertedUnitName!)
            didSelectOption(quote: coinManager.baseQuote!)
            
        } else {
            print("Selected")
            initView()
            
        }
        
    }
    
    
    @IBAction override func homeButtonWasPressed(_ sender: UIButton) {
        super.homeButtonWasPressed(sender)
    }
    
}

extension ConvertFiveUnitsVC : CoinManagerDelegate {
    
    
    func didSelectOption(quote: String) {
            
        guard let unitName = DataService.instance.getCurrencyValue(key: quote) else { return }
        
        self.title = "Convertir \(unitName)"
        self.navigationItem.hidesBackButton = true
        
        switch unitName {
        
        case "USD":
            
            coinManager.getBaseQuote(key: unitName)
            let filter = coinManager.getCoinsWithFilter(firstCoin: "MXN", secondCoin: "GBP", thirdCoin: "EUR", fourthCoin: "BTC")
            coinManager.performRequest(with: filter) { (coins) in
               
                print("Coins")
                self.updateViewWithCoinResults(coins)
                self.addReloadBarButton()
                
                
            } onError: { (error) in
                
                self.addAlert(error: error)
                
            }
            
        case "MXN" :

            coinManager.getBaseQuote(key: unitName)
            let filter = coinManager.getCoinsWithFilter(firstCoin: "USD", secondCoin: "GBP", thirdCoin: "EUR", fourthCoin: "BTC")
            coinManager.performRequest(with: filter) { (coins) in
               
                self.updateViewWithCoinResults(coins)
                self.addReloadBarButton()
                
                
            } onError: { (error) in
                
                self.addAlert(error: error)
                
            }
            
        case "GBP" :
            
            coinManager.getBaseQuote(key: unitName)
            let filter = coinManager.getCoinsWithFilter(firstCoin: "USD", secondCoin: "MXN", thirdCoin: "EUR", fourthCoin: "BTC")
            coinManager.performRequest(with: filter) { (coins) in
               
                self.updateViewWithCoinResults(coins)
                self.addReloadBarButton()
                
                
            } onError: { (error) in
                
                self.addAlert(error: error)
                
            }
            
        case "EUR" :
            
            coinManager.getBaseQuote(key: unitName)
            let filter = coinManager.getCoinsWithFilter(firstCoin: "USD", secondCoin: "MXN", thirdCoin: "GBP", fourthCoin: "BTC")
            coinManager.performRequest(with: filter) { (coins) in
               
                self.updateViewWithCoinResults(coins)
                self.addReloadBarButton()
                
                
            } onError: { (error) in
                
                self.addAlert(error: error)
                
            }
            
        case "BTC" :
            
            coinManager.getBaseQuote(key: unitName)
            let filter = coinManager.getCoinsWithFilter(firstCoin: "USD", secondCoin: "MXN", thirdCoin: "EUR", fourthCoin: "GBP")
            coinManager.performRequest(with: filter) { (coins) in
               
                self.updateViewWithCoinResults(coins)
                self.addReloadBarButton()
                
                
            } onError: { (error) in
                
                self.addAlert(error: error)
                
            }
            
            
        default:
            
            break
            
        }
        
    }
    
    func addAlert (error : String) {
        
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let action = UIAlertAction(title: "Volver", style: .default) { (action) in
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func updateViewWithCoinResults (_ coins : CoinsData) {
        
        unitToConvertTitleLabel.text = DataService.instance.getCoinTitle(value: coins.asset_id_base)
        unitToConvertTResultLabel.text = String(coinManager.number!)
        secondUnitTitleLabel.text = DataService.instance.getCoinTitle(value: coins.rates[0].asset_id_quote)
        secondUnitTResultLabel.text = String("\(coinManager.convert(coinRate: coins.rates[0].rate.round(to: 6)))")
        thirdUnitTitleLabel.text = DataService.instance.getCoinTitle(value: coins.rates[1].asset_id_quote)
        thirdUnitResultLabel.text = String("\(coinManager.convert(coinRate: coins.rates[1].rate.round(to: 6)))")
        fourthUnitTitleLabel.text = DataService.instance.getCoinTitle(value: coins.rates[2].asset_id_quote)
        fourthUnitResultLabel.text = String("\(coinManager.convert(coinRate: coins.rates[2].rate.round(to: 6)))")
        fifthUnitTitleLabel.text = DataService.instance.getCoinTitle(value: coins.rates[3].asset_id_quote)
        fifthUnitResultLabel.text = String("\(coinManager.convert(coinRate: coins.rates[3].rate.round(to: 6)))")
        updateTimeLabel.text = getDataTime()
        
    }
    
    private func addReloadBarButton () {
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadData))
        
        barButton.tintColor = .black
        self.navigationItem.rightBarButtonItem = .init(barButton)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    private func getDataTime () -> String {
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
        
    }
    
    @objc func reloadData () {
        
        coinManager.number = convertedUnitNumber
        coinManager.getBaseQuote(key: convertedUnitName!)
        didSelectOption(quote: coinManager.baseQuote!)
        
    }
    
}

