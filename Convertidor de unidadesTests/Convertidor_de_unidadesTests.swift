//
//  Convertidor_de_unidadesTests.swift
//  Convertidor de unidadesTests
//
//  Created by Carlos Hernández Vázquez on 05/03/21.
//

import XCTest

class Convertidor_de_unidadesTests: XCTestCase {
    
    private var temperature = TemperatureManager()
    private var time = TimeManager()
    private var weight = WeightManager()
    private var distance = DistanceManager()
    private var bet = BetManager()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testTemperatureManager () {
        
        //Celcius
        XCTAssert(temperature.calculate(number: 1, operation: "Celcius (°C)")[0] == "33.8")
        XCTAssert(temperature.calculate(number: 1, operation: "Celcius (°C)")[1] == "274.15")
        XCTAssert(temperature.calculate(number: 100, operation: "Celcius (°C)")[0] == "212.0")
        XCTAssert(temperature.calculate(number: 100, operation: "Celcius (°C)")[1] == "373.15")
        
        //Fahrenheit
        XCTAssert(temperature.calculate(number: 1, operation: "Fahrenheit (°F)")[0] == "-17.222223")
        XCTAssert(temperature.calculate(number: 1, operation: "Fahrenheit (°F)")[1] == "255.9278")
        XCTAssert(temperature.calculate(number: 100, operation: "Fahrenheit (°F)")[0] == "37.77778")
        XCTAssert(temperature.calculate(number: 100, operation: "Fahrenheit (°F)")[1] == "310.92783")
        
        //Kelvin
        XCTAssert(temperature.calculate(number: 1, operation: "Kelvin (K)")[0] == "-272.15")
        XCTAssert(temperature.calculate(number: 1, operation: "Kelvin (K)")[1] == "-457.86996")
        XCTAssert(temperature.calculate(number: 100, operation: "Kelvin (K)")[0] == "-173.15")
        XCTAssert(temperature.calculate(number: 100, operation: "Kelvin (K)")[1] == "-279.66998")

        
    }
    
    func testTimeManager () {
        
        //Horas
        XCTAssert(time.calculate(number: 1, operation: "Horas")[0] == "60.0")
        XCTAssert(time.calculate(number: 1, operation: "Horas")[1] == "3600.0")
        XCTAssert(time.calculate(number: 100, operation: "Horas")[0] == "6000.0")
        XCTAssert(time.calculate(number: 100, operation: "Horas")[1] == "360000.0")
        
        //Minutos
        XCTAssert(time.calculate(number: 1, operation: "Minutos")[0] == "60.0")
        XCTAssert(time.calculate(number: 1, operation: "Minutos")[1] == "0.02")
        XCTAssert(time.calculate(number: 100, operation: "Minutos")[0] == "6000.0")
        XCTAssert(time.calculate(number: 100, operation: "Minutos")[1] == "1.67")
        
        //Segundos
        XCTAssert(time.calculate(number: 1, operation: "Segundos")[0] == "0.016666668")
        XCTAssert(time.calculate(number: 1, operation: "Segundos")[1] == "0.0")
        XCTAssert(time.calculate(number: 100, operation: "Segundos")[0] == "1.6666666")
        XCTAssert(time.calculate(number: 100, operation: "Segundos")[1] == "0.03")
        
    }

    func testWeightManager () {
        
        //Kilogramos
        XCTAssert(weight.calculate(number: 1, operation: "Kilogramos")[0] == "2.205")
        XCTAssert(weight.calculate(number: 1, operation: "Kilogramos")[1] == "35.274")
        XCTAssert(weight.calculate(number: 100, operation: "Kilogramos")[0] == "220.5")
        XCTAssert(weight.calculate(number: 100, operation: "Kilogramos")[1] == "3527.4")
        
        //Libras
        XCTAssert(weight.calculate(number: 1, operation: "Libras")[0] == "0.45351475")
        XCTAssert(weight.calculate(number: 1, operation: "Libras")[1] == "16.0")
        XCTAssert(weight.calculate(number: 100, operation: "Libras")[0] == "45.351475")
        XCTAssert(weight.calculate(number: 100, operation: "Libras")[1] == "1600.0")
        
        //Onzas
        XCTAssert(weight.calculate(number: 1, operation: "Onzas")[0] == "0.028349495")
        XCTAssert(weight.calculate(number: 1, operation: "Onzas")[1] == "0.0625")
        XCTAssert(weight.calculate(number: 100, operation: "Onzas")[0] == "2.8349495")
        XCTAssert(weight.calculate(number: 100, operation: "Onzas")[1] == "6.25")
        
    }
    
    func testDistance () {
        
        //Metros
        XCTAssert(distance.calculate(number: 1, operation: "Metros")[0] == "0.9140768")
        XCTAssert(distance.calculate(number: 1, operation: "Metros")[1] == "3.281")
        XCTAssert(distance.calculate(number: 1, operation: "Metros")[2] == "39.37")
        XCTAssert(distance.calculate(number: 1, operation: "Metros")[3] == "0.00062150403")
        
        //Yardas
        XCTAssert(distance.calculate(number: 1, operation: "Yardas")[0] == "0.9140768")
        XCTAssert(distance.calculate(number: 1, operation: "Yardas")[1] == "3.0")
        XCTAssert(distance.calculate(number: 1, operation: "Yardas")[2] == "36.0")
        XCTAssert(distance.calculate(number: 1, operation: "Yardas")[3] == "0.0005681818")
        
        //Pies
        XCTAssert(distance.calculate(number: 1, operation: "Pies")[0] == "0.30478513")
        XCTAssert(distance.calculate(number: 1, operation: "Pies")[1] == "0.33333334")
        XCTAssert(distance.calculate(number: 1, operation: "Pies")[2] == "12.0")
        XCTAssert(distance.calculate(number: 1, operation: "Pies")[3] == "0.00018939393")
        
        //Pulgadas
        XCTAssert(distance.calculate(number: 1, operation: "Pulgadas")[0] == "0.025188917")
        XCTAssert(distance.calculate(number: 1, operation: "Pulgadas")[1] == "0.027777778")
        XCTAssert(distance.calculate(number: 1, operation: "Pulgadas")[2] == "0.083333336")
        XCTAssert(distance.calculate(number: 1, operation: "Pulgadas")[3] == "1.5782829e-05")
        
        //Millas
        XCTAssert(distance.calculate(number: 1, operation: "Millas")[0] == "1609.0")
        XCTAssert(distance.calculate(number: 1, operation: "Millas")[1] == "1760.0")
        XCTAssert(distance.calculate(number: 1, operation: "Millas")[2] == "5280.0")
        XCTAssert(distance.calculate(number: 1, operation: "Millas")[3] == "63360.0")
        
    }
    
    func testBetManager () {
        
        //Decimal
        XCTAssert(bet.calculateBet(amountToBet: 100, quota: "1.5", denominator: nil, option: "Cuota decimal 🇪🇺").profit == 150.0)
        XCTAssert(bet.calculateBet(amountToBet: 100, quota: "1.5", denominator: nil, option: "Cuota decimal 🇪🇺").results?[0] == "1/2")
        XCTAssert(bet.calculateBet(amountToBet: 100, quota: "1.5", denominator: nil, option: "Cuota decimal 🇪🇺").results?[1] == "-200")
        
        //American
        XCTAssert(bet.calculateBet(amountToBet: 100, quota: "1", denominator: 2, option: "Cuota fraccionaria 🇺🇸").profit == 150.0)
        XCTAssert(bet.calculateBet(amountToBet: 100, quota: "1", denominator: 2, option: "Cuota fraccionaria 🇺🇸").results?[0] == "1.5")

        
        
    }
    
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
